﻿using Common;
using Rebus.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consumer.Handlers
{
    class ProxyMessageDataHandler : IHandleMessages<MessageData2>
    {
        public async Task Handle(MessageData2 message)
        {
            //Log proxy handler completed
            Console.WriteLine("Handle Msessage data2 success");
        }
    }
}
