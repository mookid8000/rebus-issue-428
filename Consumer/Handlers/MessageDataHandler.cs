﻿using Common;
using Rebus.Handlers;
using System;
using System.Threading.Tasks;

namespace Consumer.Handlers
{
    public class MessageDataHandler : IHandleMessages<MessageData1>
    {
        // get proxy instance injected here
        public MessageDataHandler(Proxy proxy)
        {
            _proxy = proxy;
        }

        readonly Proxy _proxy;

        public async Task Handle(MessageData1 message)
        {
            //Log handler started
            Console.WriteLine("Handle Msessage data1");
            //Use Proxy to send a new message
            if (message.Id % 2 == 0)
            {
                // await so we don't exit prematurely
                await _proxy.SendMessage(message.Id);
            }

            //Lag handler completed
            Console.WriteLine("Handle Msessage data1 success");
        }
    }
}
