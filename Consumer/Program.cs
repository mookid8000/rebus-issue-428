﻿using Autofac;
using Rebus.Config;
using Rebus.Handlers;
using System;
using Common;

namespace Consumer
{
    class Program
    {
        static void Main()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            var builder = new ContainerBuilder();
            //builder.RegisterType<MessageDataHandler>();

            // register proxy as singleton
            builder.RegisterType<Proxy>().SingleInstance();

            builder.RegisterAssemblyTypes(assemblies)
                 .AssignableTo<IHandleMessages>()
                 .AsImplementedInterfaces();

            // dispose no matter what
            using (var container = builder.Build())
            {
                var activator = new Rebus.Autofac.AutofacContainerAdapter(container);

                Configure.With(activator)
                    .Transport(t => t.UseAzureServiceBus(Constants.ConnectionString, Constants.ConsumerQueue))
                    .Start();

                Console.ReadLine();
            }
        }
    }
}
