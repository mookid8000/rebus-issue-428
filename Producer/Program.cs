﻿using Rebus.Activation;
using Rebus.Config;
using System;
using Common;
using Rebus.Routing.TypeBased;

namespace Producer
{
    class Program
    {
        static void Main()
        {
            // dispose no matter what
            using (var activator = new BuiltinHandlerActivator())
            {
                Configure.With(activator)
                    .Transport(t =>
                    {
                        // changed to use producer's own queue (event though it doesn't look like
                        // it is going to receive any messages for now...)
                        t.UseAzureServiceBus(Constants.ConnectionString, Constants.ProducerQueue);
                    })
                    .Routing(r =>
                    {
                        // map the consumer as the "owner" of MessageData1 - means that it will
                        // get to receive messages of that type when they're sent using bus.Send
                        r.TypeBased()
                            .Map<MessageData1>(Constants.ConsumerQueue);
                    })
                    .Start();

                var cont = true;
                int id = 0;
                while (cont)
                {
                    id++;
                    var input = Console.ReadLine();
                    if (input == "exit")
                    {
                        cont = false;
                    }
                    else
                    {
                        // "Send" instead of "SendLocal"
                        activator.Bus.Send(new MessageData1 {Id = id, Message = $"test {id}"})
                            .Wait(); //< Wait() so we don't exit prematurely
                    }
                }
            }
        }
    }
}
