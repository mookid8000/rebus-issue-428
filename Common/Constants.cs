﻿namespace Common
{
    public static class Constants
    {
        public const string ConnectionString = "";

        public const string ConsumerQueue = "consumer";
        public const string ProducerQueue = "producer";
    }
}
