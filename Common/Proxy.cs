﻿using System;
using Rebus.Activation;
using Rebus.Config;
using System.Threading.Tasks;
using Rebus.Bus;
using Rebus.Routing.TypeBased;

namespace Common
{
    public class Proxy : IDisposable
    {
        readonly IBus _bus;

        public Proxy()
        {
            // moved bus instantiation to constructor    
            _bus = Configure.With(new BuiltinHandlerActivator())
                .Transport(t =>
                {
                    // no need to receive messages - this one can be a one-way client
                    t.UseAzureServiceBusAsOneWayClient(Constants.ConnectionString);
                })
                .Routing(r =>
                {
                    // map consumer as the owner of MessageData2
                    r.TypeBased()
                        .Map<MessageData2>(Constants.ConsumerQueue);
                })
                .Start();
        }

        // changed to be async Task so we can await things
        public async Task SendMessage(int originalMessageId)
        {
            // await so that we don't prematurely commit transactions etc.
            // and "Send" instead of "SendLocal"
            await _bus.Send(new MessageData2
            {
                Id = originalMessageId,
                Message = $"proxy {originalMessageId}"
            });
        }

        public void Dispose()
        {
            _bus.Dispose();
        }
    }
}
